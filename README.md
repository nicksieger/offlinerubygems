# Offlinerubygems

This is a simple caching reverse proxy I wrote to cache rubygems.org API
requests as well as gem files themselves, to be able to run bundler in an
offline mode.

This requires configuring a front-end such as Apache or nginx with name-based
virtual hosts to connect to the rack server, proxying requests for
http://HOSTNAME/PATH as /HOSTNAME/PATH. Your local DNS (/etc/hosts or
equivalent) must then be configured to proxy several domains through the
front-end.

For Rubygems.org, at the time this was written the domains were:

- rubygems.org
- production.s3.rubygems.org
- production.cf.rubygems.org
- bb-m.rubygems.org
