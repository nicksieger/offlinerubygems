require 'rack/cache'
require 'uri'
require 'net/http'
require 'fileutils'

module OfflineRubygems
  module_function

  class BackendNetHttpRequest
    def initialize(uri)
      @fiber = Fiber.new do
        Net::HTTP.start(uri.host, uri.port) do |http|
          http.request_get(uri.request_uri) do |resp|
            Fiber.yield resp    # initial resume yields response
            resp.read_body do |chunk|
              Fiber.yield chunk # remaining yield body chunks
            end
          end
        end
        nil
      end
    end

    def response
      @resp ||= @fiber.resume
    end

    def status
      response.code.to_i
    end

    def headers
      {}.tap do |headers|
        response.canonical_each do |k,v|
          headers[k] = v
        end
      end
    end

    def each_body_chunk
      while chunk = @fiber.resume
        yield chunk
      end
    end

    def body
      response
      enum_for(:each_body_chunk)
    end
  end

  def backend(req)
    uri = URI.parse "http:/#{req.fullpath}"
    BackendNetHttpRequest.new uri
  end

  def clean_response_headers(headers)
    headers.delete_if {|k,v| k =~ /(status|cookie)/i}
  end

  def cache_for(host)
    @metastores ||= {}
    @metastores[host] ||=
      begin
        storage = Rack::Cache::Storage.instance
        @entitystores ||= {}
        @entitystores[host] = storage.resolve_entitystore_uri "file:cache/#{host}/entity"
        storage.resolve_metastore_uri "file:cache/#{host}/meta"
      end
    @caches ||= {}
    @caches[host] ||= lambda do |*args|
      req, resp = args
      if resp
        @metastores[host].store req, resp, @entitystores[host]
      else
        @metastores[host].lookup req, @entitystores[host]
      end
    end
  end

  def call(env)
    request = Rack::Cache::Request.new(env)

    if !request.get? && !request.head?
      return [405, {"Content-Type" => "text/plain"}, ["Method Not Allowed"]]
    end

    host = request.path.split('/')[1]

    response = handle_internal request, host
    return response if response

    cached_response = cache_for(host)[request]

    if reason = serve?(cached_response)
      puts "#{reason} #{request.fullpath}"
      response = cached_response
    else
      begin
        puts "FETCH #{request.fullpath}"
        backend_response = backend(request)

        response = Rack::Cache::Response.new backend_response.status,
          clean_response_headers(backend_response.headers.dup),
          backend_response.body

        # We're actually going to cache all responses, but for ones that are
        # cacheable we will mark them as fresh for a long time with a ttl of
        # +1year
        if response.cacheable?
          response.ttl = 365 * 24 * 60 * 60
        end

        puts "STORE #{request.fullpath}"
        cache_for(host)[request, response]
        mark_connection_as_ok
      rescue
        mark_connection_as_offline
        # If the re-fetch request failed and we have a cached response, serve that
        if cached_response
          puts "ERROR (#{$!.message}) serving CACHED #{request.fullpath}"
          response = cached_response
        else
          raise
        end
      end
    end

    arr = response.to_a
    clean_response_headers(arr[1])
    arr[2] = [] if request.head?
    arr
  rescue Exception => e
    body = [e.to_s + "\n\n" + e.backtrace.join("\n")]
    puts *body
    body = [] if request.head?
    [500, {"Content-Type" => "text/plain"}, body]
  end

  def handle_internal(request, host)
    case request.path
    when "/#{host}/*invalidate*"
      puts "INVALIDATE #{host}"
      FileUtils.rm_rf "cache/#{host}"
      [200, {"Content-Type" => "text/plain"}, ["INVALIDATE #{host}"]]
    when "/*offline*"
      puts "MARK OFFLINE"
      @intervals = [0, 365 * 24 * 60]
      mark_connection_as_offline
      [200, {"Content-Type" => "text/plain"}, ["OFFLINE"]]
    when "/*online*"
      puts "MARK ONLINE"
      mark_connection_as_ok
      [200, {"Content-Type" => "text/plain"}, ["ONLINE"]]
    end
  end

  def serve?(cached_response)
    return false unless cached_response
    return "CACHED" if cached_response.fresh?
    return false unless @offline
    return "OFFLINE CACHED" unless Time.now > @next_check
  end

  def mark_connection_as_offline
    unless @offline
      @offline = true
      @intervals ||= [1, 2]
    end
    @next_check = Time.now + (@intervals.last * 60)
    @intervals = [@intervals[1], (@intervals[0] + @intervals[1])]
  end

  def mark_connection_as_ok
    @offline = false
    @intervals = nil
  end
end
