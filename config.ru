# -*- ruby -*-

require './lib/offlinerubygems.rb'

# Behave for runit
trap("TERM") do
  exit 0
end

$stdout.sync = true
run OfflineRubygems
